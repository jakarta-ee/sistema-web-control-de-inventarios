/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Properties;
import jakarta.mail.BodyPart;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
/**
 *
 * @author JHONATAN
 */
public class MailControlador implements Serializable {
    private final static String KEY_SMTP_SERVER = "mail.smtp.host";
    private final static String KEY_FROM = "mail.smtp.user";
    private final static String KEY_PASSWORD = "mail.smtp.password";

    private static Properties props;

    private static void loadConfig() {
        if (props == null) {

            props = new Properties();
            props.put(KEY_SMTP_SERVER, "smtp.gmail.com"); //El servidor SMTP de google 
           // props.put(KEY_FROM, "JhonatanBarroso1968@outlook.com"); //Correo desde vamos a enviar el correo
            props.put(KEY_FROM, "bhjhonatan2@gmail.com");
            //props.put(KEY_PASSWORD, "INFORMATICA2$bg");
            props.put(KEY_PASSWORD, "ljub hgzl xjqa kgai");//la clave de la cuenta de correo desde donde se enviara el correo
            props.put("mail.smtp.auth", "true");//Usar autenticacion mediante usuario y clave
            props.put("mail.smtp.user", "bhjhonatan2@gmail.com");
            //props.put("mail.smtp.ssl.trust", "smtp.office365.com");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
            props.put("mail.smtp.starttls.enable", "true");// para conectar de manera segura al servidor SMTP
            props.put("mail.smtp.port", "587"); //el puerto smtp seguro de google, el puerto por default es el 25

        }
    }

    public static void sendMail(String destinatario, String asunto, String cuerpo) {
        loadConfig();
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(props.getProperty(KEY_FROM)));
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(destinatario));//Se podrian añadir varios de la misma manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            
            Transport trans = session.getTransport("smtp");
            trans.connect(props.getProperty(KEY_SMTP_SERVER),
                    props.getProperty(KEY_FROM),
                    props.getProperty(KEY_PASSWORD));
            trans.sendMessage(message,message.getAllRecipients());
            trans.close();
        } catch (MessagingException  me) {
            me.printStackTrace(); //Si se produce error
            
        }
      
      
       
    }
    public static void sendMailHTML(String destinatarios, String asuntos, String cuerpoHTML) {
        loadConfig();
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(props.getProperty(KEY_FROM)));
            message.addRecipients(Message.RecipientType.TO,destinatarios);
            message.setSubject(asuntos);
            Multipart parts = new MimeMultipart();
            BodyPart bodyMail = new MimeBodyPart();
            bodyMail.setContent(cuerpoHTML, "text/html");
            parts.addBodyPart(bodyMail);
            message.setContent(parts);
            Transport transport = session.getTransport("smtp");
            transport.connect(props.getProperty(KEY_SMTP_SERVER),
                    props.getProperty(KEY_FROM),
                    props.getProperty(KEY_PASSWORD));
            transport.sendMessage(message,message.getAllRecipients());
            transport.close();
        } catch (MessagingException  me) {
            me.printStackTrace(); //Si se produce error
            
        }
      
        
       
    }
    
     public static void sendMailHTML(String destinatarios, String asuntos, String cuerpoHTML, List<File> files) throws IOException {
        loadConfig();
        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(props.getProperty(KEY_FROM)));
            message.addRecipients(Message.RecipientType.TO,destinatarios);
            message.setSubject(asuntos);
            Multipart parts = new MimeMultipart();
            BodyPart bodyMail = new MimeBodyPart();
            bodyMail.setContent(cuerpoHTML, "text/html");
            parts.addBodyPart(bodyMail);
            for (File file : files){
                MimeBodyPart attached = new MimeBodyPart();
                attached.attachFile(file);
                parts.addBodyPart(attached);   
            }
            message.setContent(parts);
            Transport transport = session.getTransport("smtp");
            transport.connect(props.getProperty(KEY_SMTP_SERVER),
                    props.getProperty(KEY_FROM),
                    props.getProperty(KEY_PASSWORD));
            transport.sendMessage(message,message.getAllRecipients());
            transport.close();
        } catch (MessagingException  me) {
            me.printStackTrace(); //Si se produce error
            
        }  
    }
}
