/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.Serializable;
import java.util.ArrayList;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;

/**
 *
 * @author JHONATAN
 */
@Named
@RequestScoped
public class Configuracion implements Serializable{
    // Tendrá el contenido que se presenta en las ventanas modales 
    private ArrayList<Dato> imagenes;

   @PostConstruct
    public void init() {
        imagenes = new ArrayList();
        imagenes.add(new Dato("https://qph.fs.quoracdn.net/main-qimg-ae5f4185487bd7d74aaba8e8d863aba0","Librería Genesis ","Library Genesis o LibGen es un motor de búsqueda que permite el libre acceso a artículos científicos y libros (la mayor parte son académicos y, en menor proporción, de ficción). Posee contenido libre en formatos PDF, EPUB, MOBI, DJVU, etc., accesible en portales de numerosas editoriales académicas, como Oxford UP, Cambridge UP, Elsevier ScienceDirect, Springer, etc.",1,"https://libgen.li/"));
        imagenes.add(new Dato("https://aretio.hypotheses.org/files/2013/09/khan2.png","Khan Academy","La Academia Khan (en inglés: Khan Academy) es una organización educativa sin ánimo de lucro creada el 16 de septiembre de 2006 por Salman Khan, egresado del Instituto de Tecnología de Massachusetts y de la Universidad de Harvard. Tiene por propósito «proporcionar una educación gratuita de nivel mundial para cualquier persona, en cualquier lugar». Es una organización de aprendizaje electrónico en línea gratuita, basada en donaciones.",2,"https://es.khanacademy.org/"));
        imagenes.add(new Dato("https://www.apestan.com/content_files/case_attached_images/65686ec90ddfec93432f57ff96c0a3e9.jpg","Capacítate para el empleo","“Capacítate para el empleo” es una iniciativa de la Fundación Carlos Slim, que ofrece en línea capacitación gratuita de oficios y ocupaciones técnico operativas con el objetivo de contribuir al desarrollo de competencias productivas.",3,"https://capacitateparaelempleo.org/"));
        imagenes.add(new Dato("https://www.unamenlinea.unam.mx/recursos/img/84453/biblioteca-nacional-de-mexico.jpg","Biblioteca Nacional de México","La Biblioteca Nacional de México, también conocida por sus siglas BNM, fue creada por decreto del 24 de octubre de 1833 por el entonces vicepresidente de México, Valentín Gómez Farías;​​ publicado el día 26 del mismo mes e incluido en el Reglamento de Instrucción Pública del 2 de junio de 1834.",4,"https://bnm.iib.unam.mx/"));
        imagenes.add(new Dato("https://www.chiquitectos.com/wp-content/uploads/2018/10/Logo-BNE.png","Biblioteca Nacional de España","La Biblioteca Nacional de España (BNE) es un organismo autónomo encargado del depósito del patrimonio bibliográfico y documental de España. Dedicada a reunir, catalogar y conservar fondos bibliográficos, custodia alrededor de treinta millones de publicaciones producidas en territorio nacional desde comienzos del siglo XVIII: libros, revistas, mapas, grabados, dibujos, partituras y folletos. ",5,"http://www.bne.es/es/Catalogos/BibliotecaDigitalHispanica/Inicio/index.html"));
        imagenes.add(new Dato("https://iconape.com/wp-content/files/vr/190565/png/190565.png","Biblioteca Virtual Miguel de Cervantes","La Biblioteca Virtual Miguel de Cervantes es una biblioteca española que reúne obras hispánicas en Internet. Tiene como objetivo principal la difusión de la cultura hispánica. Fue creada en 1999 por iniciativa de la Universidad de Alicante y con el patrocinio del Banco Santander y la Fundación Marcelino Botín.",6,"https://www.cervantesvirtual.com/"));        
    }

    public ArrayList<Dato> getImagenes() {
        return imagenes;
    }

    public void setImagenes(ArrayList<Dato> imagenes) {
        this.imagenes = imagenes;
    }
    
    
    
}
