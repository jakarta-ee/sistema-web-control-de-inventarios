/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import controlador.util.JsfUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;
import org.primefaces.model.file.UploadedFile;


/**
 *
 * @author JHONATAN
 */
@Named
@ViewScoped
public class EmailJSFManagedBeanSoporte implements Serializable{

    //Los datos del mensaje
    private String nombre;
    private String telefono;
    private String correo;
    private String mensaje;
    private UploadedFile file;
    

    public void enviarMensaje(){
        try{
           String html="<div>"
		+"<div style='background-color: black; color:white; '>"
			+"<h1>FAWARIS STORE APP SOPORTE TÉCNICO</h1>"
		+"</div>"
		+"<div>"
			+"<img style='text-align:center;' src='https://tecnologicahn.com/img/soporte-individual.png'>"
			+"<br/>"
		+"</div>"
	+"</div>";
        String tN= "<h3>Nombre solicitante:</h3>"+nombre+"<br>";
        String tT = "<h3>Teléfono:</h3>"+telefono+"<br>";
        String tC = "<h3>Dirección de correo:</h3>"+correo+"<br>";
        String tM = "<h3 style='color: #B53737;'>Notificación de Error:</h3>"+mensaje+"<br>";
        html += tN;
        html += tT;
        html += tC;
        html += tM;
        try{
            List<File> files = new ArrayList();
            files.add(copyFile(file.getFileName(),file.getInputStream()));
            MailControlador.sendMailHTML("bhjhonatan2@gmail.com","SOPORTE TÉCNICO",html,files);
        }catch(NullPointerException e){
            MailControlador.sendMailHTML("bhjhonatan2@gmail.com","SOPORTE TÉCNICO",html);
        }
        
        JsfUtil.addSuccessMessage("Mensaje enviado de Manera Exitosa");
        }catch(IOException e){
          JsfUtil.addErrorMessage("Error al enviar el mensaje !!!");
        }
    }
   
    public File copyFile(String fileName, InputStream in) {
        File f= new File(fileName);
        int read = 0;
    try {
        OutputStream out = new FileOutputStream(f);
        
        byte[] bytes = new byte[1024];
        while ((read = in.read(bytes)) != -1) {
            out.write(bytes, 0, read);
        }
        in.close();
        out.flush();
        out.close();

    } catch (IOException e) {
    System.out.println(e.getMessage());
}
    return f;
}
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }
    

	
    
}
