/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import controlador.util.JsfUtil;
import java.io.Serializable;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;


/**
 *
 * @author JHONATAN
 */
@Named
@ViewScoped
public class EmailJSFManagedBeanS implements Serializable{

    //Los datos del mensaje
    private String nombre;
    private String telefono;
    private String correo;
    private String mensaje;

    public void enviarMensaje(){
        try{
        String tN= "<strong>Nombre:</strong>"+nombre+"<br>";
        String tT = "<strong>Teléfono:</strong>"+telefono+"<br>";
        String tC = "<strong>Dirección de correo:</strong>"+correo+"<br>";
        String tM = "<strong>Mensaje:</strong>"+mensaje+"<br>";
        String img="<img src='http://www.zona-web-colombia.com/images/Dise%C3%B1o%20paginas%20web%20bogota.png' width='600px'";
        MailControlador.sendMailHTML(correo,"CONTACTO",tN+tT+tC+tM+img);
        JsfUtil.addSuccessMessage("Mensaje enviado de Manera Exitosa");
        }catch(Exception e){
          JsfUtil.addErrorMessage("Error al enviar el mensaje !!!");
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    

	
    
}
