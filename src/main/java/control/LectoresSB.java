/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package control;

import accesoDatos.DocenteFacade;
import accesoDatos.EstudianteFacade;
import accesoDatos.LibroFacade;
import accesoDatos.SolicitudFacade;
import controlador.util.JsfUtil;
import entidades.Docente;
import entidades.Estudiante;
import entidades.Lector;
import entidades.Libro;
import entidades.Solicitud;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;

/**
 *
 * @author JHONATAN
 */
@Named("lectoresSB")
@SessionScoped
public class LectoresSB implements Serializable {

    @EJB
    private SolicitudFacade solicitudFacade;

    @EJB
    private EstudianteFacade estudianteFacade;

    @EJB
    private DocenteFacade docenteFacade;

    @EJB
    private LibroFacade libroFacade;
    
    

    private List<Libro> listaLibros;
    private Libro libroD;
    private String tipo;
    private String claveS;
    private String nombreS;
    private int id;
    private Solicitud solicitud;
    private String correo;
    private boolean rend;
    
    public List<Libro> getListLibros(){
        return libroFacade.findAll();
    }

    public List<Libro> getListaLibros() {
        return listaLibros;
    }

    public void setListaLibros(List<Libro> listaLibros) {
        this.listaLibros = listaLibros;
    }
    
    public String buscarLibro(int id){
        libroD = libroFacade.find(id);
        return "/libro/ViewP.xhtml";
    }

    public Libro getLibroD() {
        return libroD;
    }

    public void setLibroD(Libro libroD) {
        this.libroD = libroD;
    }
    
    public String generarSolicitud(int idLibro, String idLector){
        rend = false;
        libroD = libroFacade.find(idLibro);
        solicitud = new Solicitud();
        solicitud.setIdLibro(libroD);
        solicitud.setStatus(3);
        obtenerDatos(idLector);
        solicitud.setIdSolicitante(new Lector(id));
        solicitud.setComentarios("Sugerencia: Número de Aula o cubículo");
        return "/publico/Create.xhtml";
    }
    
    public String enviarSolicitud(){
        guardarSolicitud();
         EmailSolicitud mail = new EmailSolicitud(claveS,nombreS, tipo, fechaA(), libroD.getId(), libroD.getTitulo(),
                 libroD.getIdEditorial().getEditorial() , libroD.getIdAutor().getAutor(),solicitud.getComentarios()+" Correo electrónico del Solicitante: "+correo);
         MailControlador.sendMailHTML("17161062@itoaxaca.edu.mx", "Soliciud de Materiales Bibliográficos", mail.generarHTMLSolicitud());
        JsfUtil.info("Solicitud de Material Bibliográfico enviada de Manera Exitosa.");
        rend=true;
         return "";
    }
    
    private String fechaA(){
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        return f.format(new Date());
    }
    
    
    
    public void guardarSolicitud(){
        Libro l = solicitud.getIdLibro();
        l.setCantidadDisponible(l.getCantidadDisponible()-1);
        libroFacade.edit(l);
        solicitudFacade.create(solicitud);
    }
    

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getClaveS() {
        return claveS;
    }

    public void setClaveS(String claveS) {
        this.claveS = claveS;
    }

    public String getNombreS() {
        return nombreS;
    }

    public void setNombreS(String nombreS) {
        this.nombreS = nombreS;
    }
    
    private Docente buscarDocente(String id){
        try{
            return docenteFacade.find(id).get(0);
        }catch(Exception e){
            return null;
        }
    }
    
    private Estudiante buscarAlumno(String id){
        try{
            return estudianteFacade.find(id).get(0);
        }catch(Exception e){
            return null;
        }
    }
    
    private void obtenerDatos(String id){
        if(buscarDocente(id)!= null){
            Docente d = buscarDocente(id);
            this.id = d.getId();
            nombreS = d.getNombre()+" "+d.getApellidoP()+" "+d.getApellidoM();
            tipo = "Docente";
            claveS = ""+d.getId();
            correo = d.getCorreo();
        }else{
            Estudiante e = buscarAlumno(id);
            this.id = e.getId();
            nombreS = e.getNombre()+" "+e.getApellidoP()+" "+e.getApellidoM();
            tipo = "Estudiante";
            claveS = e.getMatricula();
            correo = e.getCorreoE();
        }
    }
    public String getPagina1(String id){
        obtenerDatos(id);
        return "publico/Historial";
    }
    public String getPagina2(String id){
        obtenerDatos(id);
        return "/publico/Historial";
    }
    
    public List<Solicitud> obtenerListaSolicitudesLector(String idS){
        obtenerDatos(idS);
        System.out.println("El id del lector es: " +id);
        return solicitudFacade.findLector(id);
    }

    public boolean isRend() {
        return rend;
    }

    public void setRend(boolean rend) {
        this.rend = rend;
    }
    
    
    
}
