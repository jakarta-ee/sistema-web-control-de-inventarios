/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

/**
 *
 * @author JHONATAN
 */
public class Dato {
    
    private String img;
    private String titulo;
    private String descripcion;
    private String enlace;
    private int numero;

    public Dato(String img, String titulo, String descripcion, int numero) {
        this.img = img;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.numero = numero;
    }
    public Dato(String img, String titulo, String descripcion, int numero, String enlace) {
        this.img = img;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.numero = numero;
        this.enlace = enlace;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Dato{" + "img=" + img + ", titulo=" + titulo + ", descripcion=" + descripcion + ", numero=" + numero + '}';
    }
    
    public String getIDCompuesto(){
        return "portfolioModal"+numero;
    }

    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }
    
    
}
