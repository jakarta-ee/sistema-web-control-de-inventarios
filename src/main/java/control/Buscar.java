/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;

/**
 *
 * @author JHONATAN
 */
@RequestScoped
@Named
public class Buscar {
    private String palabra; // La palabra de búsqueda
    private List<Apartado> apartados;

    /**
     * Creates a new instance of buscar
     */
    public Buscar() {
    }
    @PostConstruct
    public void init() {
        apartados = new ArrayList();
        // Apartado(String enlace, String nombre, String indicaciones, String palabrasC)
        apartados.add(new Apartado ("/index.xhtml", "Dashboard","Panel de Control", "INICIO HOME DASHBOARD"));
        apartados.add(new Apartado ("/publico.xhtml", "Sección para Estudiantes y Docentes","Público", "SECCIÓN PUBLICO"));
        apartados.add(new Apartado ("/autor/List.xhtml", "Autores","Bibliografía > Autores", "AUTORES AUTOR LISTA DE AUTORES"));
        apartados.add(new Apartado ("/editorial/List.xhtml", "Editoriales","Bibliografía > Editorial", "EDITORIALES EDITORIAL LISTA DE EDITORIALES"));
        apartados.add(new Apartado ("/libro/List.xhtml", "Material Bibliográfico","Bibliografía > Libros", "MATERIAL BIBLIOGRÁFICO LIBRO LIBROS LISTA DE LIBROS"));
        apartados.add(new Apartado ("/materia/List.xhtml", "Género Literario","Bibliografía > Género", "GÉNERO GENERO TEMAS LISTA DE GÉNEROS LITERARIOS"));
        apartados.add(new Apartado ("/inventario/Download.xhtml", "Descargar Inventario","Reportes > Inventario de Libros", "INVENTARIO PDF CSV DESCARGAR EXPORTAR"));
        apartados.add(new Apartado ("/usuarios/List.xhtml", "Configuración de Usuarios","Opciones > Configuración de Usuarios", "CONFIGURACIÓN DE USUARIOS PRIVILEGIOS USUARIO"));
        apartados.add(new Apartado ("/utilerias/soporte.xhtml", "Soporte Técnico","Opciones > Soporte Técnico", "SOPORTE TÉCNICO TECNICO CONTACTO"));
        apartados.add(new Apartado ("/prestamo/FichaPrestamo.xhtml", "Préstamo de Material Bibliográfico","Herramientas > Préstamos > Ficha de Préstamo", "PRESTAR LIBRO PRÉSTAMOS PRESTAMO FICHA"));
         apartados.add(new Apartado ("/solicitud/List.xhtml", "Historial de Solicitudes de Libros","Herramientas > Préstamos > Historial", "SOLICITAR LIBRO SOLICITUD"));
        apartados.add(new Apartado ("/docente/List.xhtml", "Listado de Profesores","Dashboard > Docentes", "PROFESORES PROFESOR MAESTROS LISTA DE MAESTRO"));
        apartados.add(new Apartado ("/estudiante/List.xhtml", "Lista de Estudiantes","Dashboard > Estudiantes", "ESTUDIANTES ESTUDIANTE LISTA DE ALUMNOS ALUMNO"));
    }
    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra.toUpperCase();
    }

    public List<Apartado> getApartados() {
        return apartados;
    }

    public void setApartados(List<Apartado> apartados) {
        this.apartados = apartados;
    }
    public boolean verificar(String palabra, String frase){
        if(palabra.equals(""))
            return false;
        else
        return frase.contains(palabra);
    }
    
}
