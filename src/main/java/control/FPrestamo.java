/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package control;

import accesoDatos.DocenteFacade;
import accesoDatos.EstudianteFacade;
import accesoDatos.LibroFacade;
import accesoDatos.PrestamoFacade;
import controlador.util.JsfUtil;
import entidades.Docente;
import entidades.Estudiante;
import entidades.Lector;
import entidades.Libro;
import entidades.Prestamo;
import entidades.Usuarios;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;

import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import pdf.FichaPrestamo;

/**
 *
 * @author JHONATAN
 */
@Named("fPrestamo")
@SessionScoped
public class FPrestamo implements Serializable {

    @EJB
    private PrestamoFacade prestamoFacade;

    @EJB
    private EstudianteFacade estudianteFacade;

    @EJB
    private LibroFacade libroFacade;

    @EJB
    private DocenteFacade docenteFacade;
    
    private String idL; // Matricula del Estudiante o del Docente 
    private Lector lector;
    private ArrayList<Prestamo> librosSolicitados;
    private Libro libro;
    private String nombreS; // nombre Solicitante
    private Prestamo prestamoA;
    private String tipoL; // Tipo de Lector (Estudiante o Docente)
    
    public FPrestamo(){
        librosSolicitados = new ArrayList();
        iniciarPrestamo();
    }

    public Lector getLector() {
        return lector;
    }
    
    

    public void setLector(Lector lector) {
        this.lector = lector;
    }

    public ArrayList<Prestamo> getLibrosSolicitados() {
        return librosSolicitados;
    }

    public void setLibrosSolicitados(ArrayList<Prestamo> librosSolicitados) {
        this.librosSolicitados = librosSolicitados;
    }

    public Prestamo getPrestamoA() {
        return prestamoA;
    }

    public void setPrestamoA(Prestamo prestamoA) {
        this.prestamoA = prestamoA;
    }

    public String getTipoL() {
        return tipoL;
    }

    public void setTipoL(String tipoL) {
        this.tipoL = tipoL;
    }
    
    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public String getIdL() {
        return idL;
    }

    public void setIdL(String idL) {
        this.idL = idL;
    }

    public String getNombreS() {
        return nombreS;
    }

    public void setNombreS(String nombreS) {
        this.nombreS = nombreS;
    }
    
   

    public void guardarPrestamos(){
        
    }
    
    public List<Libro> getLibrosDisponibles(){
        return libroFacade.findAll();
    }
    
    private Estudiante buscarEstudiante(String matricula){
        Estudiante e = null;
        try{
           e = estudianteFacade.buscarMatricula(matricula).get(0);
        }catch(Exception ex){}
        return e;
    }
    private Docente buscarDocente(String clave){
        Docente d = null;
        try{
            docenteFacade.find(Integer.parseInt(clave));
        }catch(NumberFormatException ex){}
        return d;
    }
    public void buscarNombreSolicitante(){
        nombreS = "Lector No Encontrado";
        Estudiante e = buscarEstudiante(idL);
        if(e != null){
            nombreS = e.getNombre()+" "+e.getApellidoP() +" "+ e.getApellidoM();
            lector = new Lector();
            System.out.println("El id del Lector es : "+e.getId());
            lector.setIdlector(e.getId());
            System.out.println("El id del Lector es : "+lector.getIdlector());
            tipoL = "Estudiante";
        }else if(buscarDocente(idL)!= null){
            Docente d = buscarDocente(idL);
            nombreS = d.getNombre()+" "+d.getApellidoP() +" "+ d.getApellidoM();
            lector = new Lector();
            lector.setIdlector(d.getId());
            tipoL = "Docente";
        }
         
    }
    private void iniciarPrestamo(){
        prestamoA = new Prestamo();
        libro = new Libro();
        libro.setCantidadDisponible(1);
        prestamoA.setFechaPrestamo(new Date());
        Date actual = new Date();
        prestamoA.setFechaDevolucion(new Date(actual.getTime() + TimeUnit.DAYS.toMillis( 1 )));
        prestamoA.setObservacion("Sin Observaciones");
        prestamoA.setStatus(1);
        prestamoA.setCantidad(1);
    }
     public void agregarLibro(){
        Boolean flag = true;

            for(Prestamo p : librosSolicitados){
                if((int)p.getIdLibro().getId() == (int)libro.getId()){
                    p.setCantidad(p.getCantidad() + prestamoA.getCantidad());
                    p.setFechaDevolucion(prestamoA.getFechaDevolucion());
                    flag = false;
                    JsfUtil.info("El Libro: "+libro.getTitulo()+" fue actualizado.");
                    iniciarPrestamo();
                    break;
                }
            }
            if(flag){
                prestamoA.setIdLibro(libro);
                prestamoA.setIdSolicitante(lector);
                librosSolicitados.add(prestamoA);
                JsfUtil.info("El libro: "+libro.getTitulo()+" fue agregado.");
                iniciarPrestamo();
            }
            
        
    }
     public Date getMinDate(){
         return new Date();
     }
     
     public boolean getContinuar(){
         return lector == null || librosSolicitados.isEmpty();
     }
     
     public void eliminarLibro(Prestamo p){
       librosSolicitados.remove(p);
       JsfUtil.info("El Libro: "+p.getIdLibro().getTitulo()+" Fue eliminado");  
     }
     
     public void cancelar(){
         lector = null;
         idL = null;
         nombreS = null;
         librosSolicitados = new ArrayList();
         iniciarPrestamo();
     }
     
     public void finalizarPrestamo(Usuarios user){
         try {
                //Registrar los datos a la DB
                registrarDatos();
                JsfUtil.info("Prestamo registrado de Manera Exitosa :) .");
                // Generar Ficha de Prestamo
                FichaPrestamo t = new FichaPrestamo(librosSolicitados, idL,tipoL,nombreS,user.getNombre(),""+user.getId());
                downloadFile();
                cancelar();
            } catch (IOException ex) {
                Logger.getLogger(FPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            }
     }
     public void downloadFile() throws IOException {

        FileInputStream fis = null;
        try {
            File fichero= new File("Comprobante.pdf");
            FacesContext ctx = FacesContext.getCurrentInstance();
            fis = new FileInputStream(fichero);
            byte[] bytes = new byte[1000];
            int read = 0;
            if (!ctx.getResponseComplete()) {
                String fileName = fichero.getName();
                //String contentType = "application/vnd.ms-excel";
                String contentType = "application/pdf";
                HttpServletResponse response =
                        (HttpServletResponse) ctx.getExternalContext().getResponse();
                
                response.setContentType(contentType);
                
                response.setHeader("Content-Disposition",
                        "attachment;filename=\"" + fileName + "\"");
                
                ServletOutputStream out = response.getOutputStream();
                
                while ((read = fis.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                System.out.println("\nDescargado\n");
                ctx.responseComplete();
            }       
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                     fis.close();
                } catch (IOException ex) {
                         Logger.getLogger(FPrestamo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

}
     
     public void registrarDatos(){
         for(Prestamo p: librosSolicitados){
             // Actualizar la cantidad de libros 
             Libro l = p.getIdLibro();
             l.setCantidadDisponible(l.getCantidadDisponible()-p.getCantidad());
             libroFacade.edit(l);
             p.setIdSolicitante(lector);
             prestamoFacade.create(p);
         }
     }
    
}
