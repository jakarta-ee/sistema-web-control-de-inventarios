/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package control;

import java.io.Serializable;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;

/**
 *
 * @author JHONATAN
 */
@Named("filtroTablaLibro")
@ViewScoped
public class FiltroTablaLibro implements Serializable {

    private boolean activo;
    
    public void toggleGlobalFilter() {
        setActivo(!isActivo());
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    
    
}
