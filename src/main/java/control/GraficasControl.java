/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/CDI/Scope.java to edit this template
 */
package control;

import accesoDatos.LibroFacade;
import accesoDatos.PrestamoFacade;
import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DonutChartModel;
import org.primefaces.model.chart.PieChartModel;



/**
 *
 * @author JHONATAN
 */
@Named("graficasControl")
@RequestScoped
public class GraficasControl implements Serializable {
    
    @EJB
    private PrestamoFacade prestamoFacade;
  
    @EJB
    private LibroFacade libroFacade;
    
    private BarChartModel barModel;
    private DonutChartModel donutModel;
    private PieChartModel pieModel1;
    
    @PostConstruct
    public void init() {
        createBarModels();
        createDonutModels();
        createPieModels();
    }

    private List<Object[]> top5Libros(){
        return prestamoFacade.executeQuery("SELECT id_libro, count(*) as repeticiones FROM prestamo group by id_libro order by repeticiones desc limit 5;");
    }
    public List<Object[]> datostop5Libros(){
        List<Object[]> libros = top5Libros();
        for(Object[] dato: libros){
            Integer id = Integer.valueOf(""+dato[0]);
            String titulo = libroFacade.find(id).getTitulo();
            dato[0]= titulo;
        }
        return libros;
    }
    public List<Object[]> datostop5Autores(){
        List<Object[]> libros = top5Libros();
        for(Object[] dato: libros){
            Integer id = Integer.valueOf(""+dato[0]);
            String autor = libroFacade.find(id).getIdAutor().getAutor();
            dato[0]= autor;
        }
        return libros;
    }
    
    /** Gráfica de barras ***/
    private void createBarModels() {
        createBarModel();
        //createHorizontalBarModel();
    }
    public void itemSelect(ItemSelectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected", "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    private void createBarModel() {
        barModel = initBarModel();
        barModel.setAnimate(true);
        barModel.setSeriesColors("8DF086,F0C56E,5693F0,F062B5");
        barModel.setTitle("Top 5 de los libros más leídos con base a los préstamos registrados.");
        barModel.setLegendPosition("ne");

        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("Libro");

        Axis yAxis = barModel.getAxis(AxisType.Y);
        yAxis.setLabel("Núm. Prestamos");
        yAxis.setMin(0);
        yAxis.setMax(100);
    }
    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();

        ChartSeries top5 = new ChartSeries();
        List<Object[]> libros = datostop5Libros();
        
        top5.setLabel("Número de prestamos por libro");
        top5.set(libros.get(0)[0], Integer.valueOf(""+libros.get(0)[1]));
        top5.set(libros.get(1)[0], Integer.valueOf(""+libros.get(1)[1]));
        top5.set(libros.get(2)[0], Integer.valueOf(""+libros.get(2)[1]));
        top5.set(libros.get(3)[0], Integer.valueOf(""+libros.get(3)[1]));
        top5.set(libros.get(4)[0], Integer.valueOf(""+libros.get(4)[1]));

        model.addSeries(top5);

        return model;
    }



    public BarChartModel getBarModel() {
        return barModel;
    }

    public void setBarModel(BarChartModel barModel) {
        this.barModel = barModel;
    }
    
    /*** Gráfica Dona**/
    
    
    private void createDonutModels() {
        donutModel = initDonutModel();
        donutModel.setTitle("Top 5 de los libros más leídos con base a los préstamos registrados.");
        donutModel.setLegendPosition("w");
        donutModel.setSliceMargin(5);
        //donutModel.setDataFormat("value");
        donutModel.setShowDataLabels(true);
        donutModel.setShadow(false);
    }
 
    private DonutChartModel initDonutModel() {
        DonutChartModel model = new DonutChartModel();
        List<Object[]> libros = datostop5Libros();
        Map<String, Number> circle1 = new LinkedHashMap<>();
        circle1.put(""+libros.get(0)[0], Integer.valueOf(""+libros.get(0)[1]));
        circle1.put(""+libros.get(1)[0], Integer.valueOf(""+libros.get(1)[1]));
        circle1.put(""+libros.get(2)[0], Integer.valueOf(""+libros.get(2)[1]));
        circle1.put(""+libros.get(3)[0], Integer.valueOf(""+libros.get(3)[1]));
        circle1.put(""+libros.get(4)[0], Integer.valueOf(""+libros.get(4)[1]));
        model.addCircle(circle1);
 
        return model;
    }

    public DonutChartModel getDonutModel() {
        return donutModel;
    }

    public void setDonutModel(DonutChartModel donutModel) {
        this.donutModel = donutModel;
    }
    
    
    /** Fin de la Gráfica Polar**/
    
    /** Gráfica de Pastel **/
    private void createPieModels() {
        createPieModel1();
    }
    private void createPieModel1() {
        pieModel1 = new PieChartModel();
        pieModel1.setDiameter(170);
        pieModel1.setSeriesColors("8DF086,FF7F50,F0C56E,5693F0,F062B5");
        List<Object[]> libros = datostop5Autores();
        pieModel1.set(""+libros.get(0)[0], Integer.valueOf(""+libros.get(0)[1]));
        pieModel1.set(""+libros.get(1)[0], Integer.valueOf(""+libros.get(1)[1]));
        pieModel1.set(""+libros.get(2)[0], Integer.valueOf(""+libros.get(2)[1]));
        pieModel1.set(""+libros.get(3)[0], Integer.valueOf(""+libros.get(3)[1]));
        pieModel1.set(""+libros.get(4)[0], Integer.valueOf(""+libros.get(4)[1]));

        pieModel1.setTitle("¡Descubre cuáles son los autores de libros más populares y demandados! ");
        pieModel1.setLegendPosition("w");
        pieModel1.setShadow(false);
    }
    
    
    /*public void imprimir(){
         List<Object[]> libros = datostop5Libros();
          System.out.println("Tamaño de la lista "+libros.size());
        for(Object[] dato: libros){
            System.out.println("Libro: "+dato[0]);
            System.out.println("Total "+dato[1]);
            System.out.println();
        }
    }*/

    public PieChartModel getPieModel1() {
        return pieModel1;
    }

    public void setPieModel1(PieChartModel pieModel1) {
        this.pieModel1 = pieModel1;
    }
}
