/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entidades;

import java.io.Serializable;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

/**
 *
 * @author JHONATAN
 */
@Entity
@Table(name = "lectores")
@NamedQueries({
    @NamedQuery(name = "Lectores.findAll", query = "SELECT l FROM Lectores l"),
    @NamedQuery(name = "Lectores.findByIdlector", query = "SELECT l FROM Lectores l WHERE l.idlector = :idlector"),
    @NamedQuery(name = "Lectores.findByNombre", query = "SELECT l FROM Lectores l WHERE l.nombre = :nombre"),
    @NamedQuery(name = "Lectores.findByApellidoP", query = "SELECT l FROM Lectores l WHERE l.apellidoP = :apellidoP"),
    @NamedQuery(name = "Lectores.findByApellidoM", query = "SELECT l FROM Lectores l WHERE l.apellidoM = :apellidoM")})
public class Lectores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idlector")
    private int idlector;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "apellidoP")
    private String apellidoP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "apellidoM")
    private String apellidoM;

    public Lectores() {
    }

    public int getIdlector() {
        return idlector;
    }

    public void setIdlector(int idlector) {
        this.idlector = idlector;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoP() {
        return apellidoP;
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    public String getApellidoM() {
        return apellidoM;
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = apellidoM;
    }
    
}
