/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package controlador;

import accesoDatos.AutorFacade;
import accesoDatos.EditorialFacade;
import accesoDatos.LibroFacade;
import archivos.InventarioCSV;
import entidades.Autor;
import entidades.Editorial;
import entidades.Libro;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.ejb.EJB;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import pdf.InventarioAutoresPDF;
import pdf.InventarioEditorialPDF;
import pdf.InventarioPDF;

/**
 *
 * @author JHONATAN
 */
@Named("inventarioController")
@SessionScoped
public class InventarioController implements Serializable {

    @EJB
    private LibroFacade libroFacade;

    @EJB
    private EditorialFacade editorialFacade;

    @EJB
    private AutorFacade autorFacade;
    
    private boolean activar; // Otras formas de descarga
    
    public int getNumeroLibros(){
        return libroFacade.count();
    }
    public int getNumeroEditoriales(){
        return editorialFacade.count();
    }
    public int getNumeroAutores(){
        return autorFacade.count();
    }
    
     public void downloadFile() throws IOException {
            
            FileInputStream fis = null;
            try {
                InventarioCSV csv = new InventarioCSV(libroFacade.findAll());
                csv.generaCSV();
                File fichero= new File("Inventario.csv");
                FacesContext ctx = FacesContext.getCurrentInstance();
                fis = new FileInputStream(fichero);
                byte[] bytes = new byte[1000];
                int read = 0;
            if (!ctx.getResponseComplete()) {
                String fileName = fichero.getName();
                String contentType = "text/comma-separated-values";
                HttpServletResponse response =
                        (HttpServletResponse) ctx.getExternalContext().getResponse();
                
                response.setContentType(contentType);
                
                response.setHeader("Content-Disposition",
                        "attachment;filename=\"" + fileName + "\"");
                
                ServletOutputStream out = response.getOutputStream();
                
                while ((read = fis.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                System.out.println("\nDescargado\n");
                ctx.responseComplete();
            }       
            } catch (FileNotFoundException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
}

public void downloadFilePDF() throws IOException {

        FileInputStream fis = null;
        try {
            InventarioPDF pdf = new InventarioPDF(libroFacade.findAll());
            File fichero= new File("Inventario.pdf");
            FacesContext ctx = FacesContext.getCurrentInstance();
            fis = new FileInputStream(fichero);
            byte[] bytes = new byte[1000];
            int read = 0;
            if (!ctx.getResponseComplete()) {
                String fileName = fichero.getName();
                //String contentType = "application/vnd.ms-excel";
                String contentType = "application/pdf";
                HttpServletResponse response =
                        (HttpServletResponse) ctx.getExternalContext().getResponse();
                
                response.setContentType(contentType);
                
                response.setHeader("Content-Disposition",
                        "attachment;filename=\"" + fileName + "\"");
                
                ServletOutputStream out = response.getOutputStream();
                
                while ((read = fis.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                System.out.println("\nDescargado\n");
                ctx.responseComplete();
            }       
            } catch (FileNotFoundException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    try {
                        fis.close();
                    } catch (IOException ex) {
                        Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
        }
    //Formas de descarga
    public void mostrarF(){
        activar = !activar;
    }

    public boolean isActivar() {
        return activar;
    }

    public void setActivar(boolean activar) {
        this.activar = activar;
    }
    
    
    public List<List<Libro>> ordenarEditorial(){
        List<List<Libro>> lista = new ArrayList<>();
                for(Editorial e : editorialFacade.findEditorialASC())
                    lista.add(libroFacade.findEditorial(e.getEditorial()));
        return lista;
    }
    
    public List<List<Libro>> ordenarAutor(){
        List<List<Libro>> lista = new ArrayList<>();
                for(Autor a : autorFacade.findAutorASC())
                    lista.add(libroFacade.findAutor(a.getAutor()));
        return lista;
    }

   public void downloadFileAutorPDF() throws IOException {

        FileInputStream fis = null;
        try {
            InventarioAutoresPDF pdf = new InventarioAutoresPDF(ordenarAutor());
            File fichero= new File("InventarioAutor.pdf");
            FacesContext ctx = FacesContext.getCurrentInstance();
            fis = new FileInputStream(fichero);
            byte[] bytes = new byte[1000];
            int read = 0;
            if (!ctx.getResponseComplete()) {
                String fileName = fichero.getName();
                //String contentType = "application/vnd.ms-excel";
                String contentType = "application/pdf";
                HttpServletResponse response =
                        (HttpServletResponse) ctx.getExternalContext().getResponse();
                
                response.setContentType(contentType);
                
                response.setHeader("Content-Disposition",
                        "attachment;filename=\"" + fileName + "\"");
                
                ServletOutputStream out = response.getOutputStream();
                
                while ((read = fis.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                System.out.println("\nDescargado\n");
                ctx.responseComplete();
            }       
            } catch (FileNotFoundException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    try {
                        fis.close();
                    } catch (IOException ex) {
                        Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
        }
    
    public void downloadFileEditorialPDF() throws IOException {

        FileInputStream fis = null;
        try {
            System.out.println("Preparando la descarga ....");
            InventarioEditorialPDF pdf = new InventarioEditorialPDF(ordenarEditorial());
            File fichero= new File("InventarioEditorial.pdf");
            FacesContext ctx = FacesContext.getCurrentInstance();
            fis = new FileInputStream(fichero);
            byte[] bytes = new byte[1000];
            int read = 0;
            if (!ctx.getResponseComplete()) {
                String fileName = fichero.getName();
                //String contentType = "application/vnd.ms-excel";
                String contentType = "application/pdf";
                HttpServletResponse response =
                        (HttpServletResponse) ctx.getExternalContext().getResponse();
                
                response.setContentType(contentType);
                
                response.setHeader("Content-Disposition",
                        "attachment;filename=\"" + fileName + "\"");
                
                ServletOutputStream out = response.getOutputStream();
                
                while ((read = fis.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                System.out.println("\nDescargado\n");
                ctx.responseComplete();
            }       
            } catch (FileNotFoundException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    try {
                        fis.close();
                    } catch (IOException ex) {
                        Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
        }
}
