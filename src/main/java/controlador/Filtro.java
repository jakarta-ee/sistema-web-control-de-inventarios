
package controlador;

import java.io.IOException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author JHONATAN
 */
@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.xhtml" })
public class Filtro implements Filter {
    
    @Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {

			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession ses = reqt.getSession(false);

			String reqURI = reqt.getRequestURI();
			if (reqURI.contains("/login.xhtml") || (ses != null && ses.getAttribute("username") != null)
					|| reqURI.contains("/public/")
					|| reqURI.contains("javax.faces.resource"))
				chain.doFilter(request, response);
                        else 
				resp.sendRedirect(reqt.getContextPath() + "/faces/login.xhtml");
                        
		} catch (IOException | ServletException e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void destroy() {

	}
    
}
