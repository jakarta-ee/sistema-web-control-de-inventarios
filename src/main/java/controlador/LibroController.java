package controlador;

import entidades.Libro;
import controlador.util.JsfUtil;
import controlador.util.PaginationHelper;
import accesoDatos.LibroFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import jakarta.ejb.EJB;
import jakarta.inject.Named;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.FacesConverter;
import jakarta.faces.model.DataModel;
import jakarta.faces.model.ListDataModel;
import jakarta.faces.model.SelectItem;



@Named("libroController")
@SessionScoped
public class LibroController implements Serializable {

    private Libro current;
    private DataModel items = null;
    @EJB
    private accesoDatos.LibroFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private String search;
    private boolean advertencia;
    private String titulo;

    public LibroController() {
        search = "";
        //advertencia = true;
    }

    public Libro getSelected() {
        if (current == null) {
            current = new Libro();
            selectedItemIndex = -1;
        }
        return current;
    }

    private LibroFacade getFacade() {
        return ejbFacade;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
    

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(5) {

                @Override
                public int getItemsCount() {
                    return getFacade().find(search).size();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()-1},search));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Libro) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        advertencia = false;
        current = new Libro();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        List<Libro> libros = ejbFacade.find(current.getTitulo(), current.getIdAutor().getAutor(), current.getIdEditorial().getEditorial(), current.getIsbn());
        
        if(libros.isEmpty()){
        try {
            advertencia = false;
            if(current.getCantidadDisponible()>0)
                current.setStatus(1);
            else 
                current.setStatus(0);
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LibroCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }}else{
            JsfUtil.addErrorMessage(new Exception(), "Error de duplicidad, el libro ya esta registrado con los mismos valores.");
            advertencia = true;
            current = libros.get(0);
            return null;
        }
        
    }

    public String prepareEdit() {
        current = (Libro) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            Libro l = getFacade().find(current.getId());
            int cant = current.getCantidad()-l.getCantidad();
            System.out.println("La cantidad es: "+ cant);
            current.setCantidadDisponible(current.getCantidadDisponible()+cant);
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LibroUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Libro) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }
    public String show() {
        recreateModel();
        getItems();
        return "List";
    }
    public String showAll() {
        search="";
        recreateModel();
        getItems();
        return "List";
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.info(ResourceBundle.getBundle("/Bundle").getString("LibroDeleted"));
        } catch (Exception e) {
            JsfUtil.error(ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Libro getLibro(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Libro.class)
    public static class LibroControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            LibroController controller = (LibroController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "libroController");
            return controller.getLibro(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Libro) {
                Libro o = (Libro) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Libro.class.getName());
            }
        }
        

    }
    public String salir(){
        advertencia = false;
        return "List";
    }
    public String actualizar(){
        advertencia = false;
        return "EditExistencias";
    }

    public boolean isAdvertencia() {
        return advertencia;
    }

    public void setAdvertencia(boolean advertencia) {
        this.advertencia = advertencia;
    }
    
    public void comprobarL(){
        List<Libro> libros = ejbFacade.find(current.getTitulo(), current.getIdAutor().getAutor());
        advertencia = !libros.isEmpty();
        if(!libros.isEmpty())
            current = libros.get(0);
    }

    public int getSelectedItemIndex() {
        return selectedItemIndex;
    }

    public void setSelectedItemIndex(int selectedItemIndex) {
        this.selectedItemIndex = selectedItemIndex;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    


    
    

}
