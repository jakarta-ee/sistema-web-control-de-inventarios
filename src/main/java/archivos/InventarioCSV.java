/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package archivos;

import entidades.Libro;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author JHONATAN
 */
public class InventarioCSV {
    
    private List<Libro> libros;
    private final String[] encabezados={"No.","IDENTIFICADOR","TÍTULO","TOTAL","CANTIDAD DISPONIBLE","EDITORIAL", "AUTOR"};
    
    public InventarioCSV(List<Libro> mlibros){
        this.libros = mlibros;
    }
    public void generaCSV() throws IOException{
        try (FileWriter csvWriter = new FileWriter("Inventario.csv")) {
            for(String s: encabezados){
                csvWriter.append(s);
                csvWriter.append(",");
            }
            csvWriter.append("\n");
            for(int x=0; x< libros.size(); x++){
                Libro libro = libros.get(x);
                csvWriter.append(x+1+",");
                csvWriter.append(libro.getId()+",");
                csvWriter.append(libro.getTitulo()+",");
                csvWriter.append(libro.getCantidad()+",");
                csvWriter.append(libro.getCantidadDisponible()+",");
                csvWriter.append(libro.getIdEditorial().getEditorial()+",");
                csvWriter.append(libro.getIdAutor().getAutor());
                csvWriter.append("\n");
            }
            csvWriter.flush();
        }
        
    }
    
}
