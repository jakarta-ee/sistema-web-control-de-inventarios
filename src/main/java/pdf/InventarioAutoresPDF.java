package pdf;

import com.koadweb.javafpdf.Alignment;
import com.koadweb.javafpdf.Borders;
import com.koadweb.javafpdf.Color;
import com.koadweb.javafpdf.Coordinate;
import com.koadweb.javafpdf.FPDF;
import com.koadweb.javafpdf.FontStyle;
import com.koadweb.javafpdf.ImageType;
import com.koadweb.javafpdf.Orientation;
import com.koadweb.javafpdf.Position;
import entidades.Libro;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JHONATAN
 */
public class InventarioAutoresPDF extends FPDF {
    private List<List<Libro>> libros;
    private final String[] encabezados={"No.","ID","Título","Total","Disponible","Editorial"};
    
    public InventarioAutoresPDF(List<List<Libro>> libros){
        super(Orientation.PORTRAIT);
        author = "JHONATAN BARROSO";
        creator = "FAWARIS BOOK APP";
        this.libros = libros;
        generarInventario();
    }
    
    @Override
    public void Footer() {
        
        // Posición: a 1,5 cm del final
        this.setY(-15);
        try {
            // Arial italic 8
            this.setFont("Arial",null,8);
            // Número de página "Página "+this.pageNo()+
            this.Cell(0, 10,"Página "+this.pageNo(), Position.BELOW, Alignment.LEFT);
        } catch (IOException ex) {
            Logger.getLogger(PDF.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    
    @Override
    public void Header(){
        
        
    }
    public void encabezado(){
        try {
            HashSet<FontStyle> style = new HashSet();
            style.add(FontStyle.BOLD);
            
            this.Image("C:/www/cecyte.png",new Coordinate(0,0),60,40,ImageType.PNG,0);

            //Arial bold 15
            this.setFont("Arial",style,12);
            //Movernos a la derecha
            this.Cell(60,0);
            this.setFillColor(255,255,255);
            this.setDrawColor(29,224,153);
            this.setLineWidth((float)0.3);
            this.setTextColor(new Color(0));
            this.Cell(130,10,"CECyTE Plantel 25 \"SAN PABLO HUIXTEPEC\"",new Borders(true,true,true,true),Position.BELOW, Alignment.CENTER,true, 0);
            this.setFont("Times",style,14);
            this.setFillColor(29,224,153);
            this.setTextColor(new Color(0));
            this.Cell(130, 10,"CENTRO DE INFORMACIÓN",new Borders(true,true,true,true),Position.BELOW, Alignment.CENTER,true, 0);
            this.setTextColor(new Color(0));
            this.Ln();
            this.Ln();
        } catch (IOException ex) {
            Logger.getLogger(InventarioAutoresPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cuerpoInventario() throws IOException{
        encabezado();
        HashSet<FontStyle> style = new HashSet();
            style.add(FontStyle.BOLD);
        this.setFont("Times",null,14);
        //String strDateFormat = "dd/MM/yy"; // El formato de fecha está especificado  
        //SimpleDateFormat fecha = new SimpleDateFormat(strDateFormat);
        DateFormat fecha = DateFormat.getDateInstance(DateFormat.LONG);
        this.Cell(180,10,"San Pablo Huixtepec, Oaxaca de Juárez, Oaxaca a "+fecha.format(new Date())+"." ,Position.NEXTLINE, Alignment.RIGHT);
        this.Ln();
       
        this.setFont("Times",style,16);
        this.Cell(200,10,"INVENTARIO DE LIBROS POR AUTOR",Position.NEXTLINE, Alignment.CENTER);
        this.Ln();
        for(List<Libro> l: libros){
            if(!l.isEmpty())
                generarTabla(l);
        }
        this.Ln(25);
        this.setFont("Times",style,10);
        this.setX(75);
        this.Cell(60,7,"MAE. Pedro Lorenzana Carballo",new Borders(false,true,false,false),Position.BELOW, Alignment.CENTER,false, 0);
        this.setX(75);
        this.Cell(60,7,"DIRECTOR DEL PLANTEL",Position.BELOW, Alignment.CENTER);
    }
    public void generarTabla(List<Libro> mlista) throws IOException{
        HashSet<FontStyle> style = new HashSet();
            style.add(FontStyle.BOLD);
        // Colores, ancho de línea y fuente en Negrita
		//this.setFillColor(29,201,206);
                //this.setFillColor(29,224,153);
                this.setFont("Times",style,12);
                this.setX(75);
                this.Cell(60,7,mlista.get(0).getIdAutor().getAutor(),new Borders(false,false,false,false),Position.BELOW, Alignment.CENTER,false, 0);
                this.setX(75);
                this.Ln();
                this.setFillColor(238,238,238);
		this.setTextColor(new Color(0));
		this.setDrawColor(new Color(0));
		this.setLineWidth((float)0.3);
		this.setFont("Times",style,10);
		//Cabecera
		int w[] = {8, 8,76,12, 20, 40};
		this.setX(22);
		for(int i=0; i < encabezados.length; i++) 
                    this.Cell(w[i],7,encabezados[i],new Borders(true,true,true,true),Position.RIGHTOF, Alignment.CENTER,true, 0);                     
		this.Ln();
		//Restauración de colores
		//this.setFillColor(224,235,255);
                this.setFillColor(252,252,252);
		this.setTextColor(new Color(0));
		this.setFont("Times",null,9);
		//Datos
		boolean fill=false;
		this.setX(22);

                 for(int x=0; x<mlista.size();x++){
			Libro libro = mlista.get(x);
			this.Cell(w[0],6,""+(x+1),new Borders(true,true,true,true),Position.RIGHTOF, Alignment.CENTER,fill, 0);
			this.Cell(w[1],6,""+libro.getId(),new Borders(true,true,true,true),Position.RIGHTOF, Alignment.CENTER,fill, 0);
			this.Cell(w[2],6,libro.getTitulo(),new Borders(true,true,true,true),Position.RIGHTOF, Alignment.LEFT,fill, 0);
			this.Cell(w[3],6,""+libro.getCantidad(),new Borders(true,true,true,true),Position.RIGHTOF, Alignment.CENTER,fill, 0);
			this.Cell(w[4],6,""+libro.getCantidadDisponible(),new Borders(true,true,true,true),Position.RIGHTOF, Alignment.CENTER,fill, 0);
                        this.Cell(w[5],6,libro.getIdEditorial().getEditorial(),new Borders(true,true,true,true),Position.RIGHTOF, Alignment.LEFT,fill, 0);
			//this.Cell(w[6],6,libro.getIdAutor().getAutor(),new Borders(true,true,true,true),Position.RIGHTOF, Alignment.LEFT,fill, 0);
			this.Ln();
			this.setX(22);
			fill = !fill;
		}
		//Línea de cierre
		this.Cell(getSumaAncho(w),0,"",Position.RIGHTOF,Alignment.JUSTIFIED);
                this.Ln(15);

	}
    private int getSumaAncho(int[] s){
        int suma = 0;
        for(int x: s){
            suma += x;
        }
        return suma;
    }
    
     private void generarInventario(){
        try {
            addPage();
            cuerpoInventario();
            output(new File("InventarioAutor.pdf"));
        } catch (IOException ex) {
            Logger.getLogger(InventarioAutoresPDF.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

}
