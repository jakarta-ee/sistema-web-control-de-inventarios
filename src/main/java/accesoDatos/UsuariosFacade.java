/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Usuarios;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    public List<Usuarios> buscar(String usuario, String password){
        return em.createQuery(
        "SELECT u FROM Usuarios u WHERE u.usuario = ?1 AND u.clave = ?2")
        .setParameter(1, usuario)
        .setParameter(2, password)
        .getResultList();
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }
    
}
