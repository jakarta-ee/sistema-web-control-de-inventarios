/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Autor;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class AutorFacade extends AbstractFacade<Autor> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AutorFacade() {
        super(Autor.class);
    }
    public List<Autor> find(String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        jakarta.persistence.criteria.Root<Autor> rt = cq.from(Autor.class);
        cq.select(rt);
        cq.where(getEntityManager().getCriteriaBuilder().or(
            getEntityManager().getCriteriaBuilder().equal(rt.get("id"), id),
            getEntityManager().getCriteriaBuilder().like(rt.get("autor"), "%"+search+"%")
        ));
        jakarta.persistence.Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
    public List<Autor> findRange(int[] range, String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        jakarta.persistence.criteria.Root<Autor> rt = cq.from(Autor.class);
        cq.select(rt);
        cq.where(getEntityManager().getCriteriaBuilder().or(
            getEntityManager().getCriteriaBuilder().equal(rt.get("id"), id),
            getEntityManager().getCriteriaBuilder().like(rt.get("autor"), "%"+search+"%")
        ));
        jakarta.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    public List<Autor> findAutorASC(){
        return em.createQuery(
        "SELECT a FROM Autor a ORDER BY a.autor ASC")
        .getResultList();
    }
    
}
