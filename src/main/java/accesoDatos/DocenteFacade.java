/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Docente;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class DocenteFacade extends AbstractFacade<Docente> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DocenteFacade() {
        super(Docente.class);
    }
     public List<Docente> find(String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        jakarta.persistence.criteria.Root<Docente> rt = cq.from(Docente.class);
        cq.select(rt);
        cq.where(getEntityManager().getCriteriaBuilder().or(
            getEntityManager().getCriteriaBuilder().equal(rt.get("id"), id),
            getEntityManager().getCriteriaBuilder().like(rt.get("nombre"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("apellidoP"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("apellidoM"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("area"), "%"+search+"%")
        ));
        jakarta.persistence.Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
    public List<Docente> findRange(int[] range, String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        jakarta.persistence.criteria.Root<Docente> rt = cq.from(Docente.class);
        cq.select(rt);
        cq.where(getEntityManager().getCriteriaBuilder().or(
            getEntityManager().getCriteriaBuilder().equal(rt.get("id"), id),
            getEntityManager().getCriteriaBuilder().like(rt.get("nombre"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("apellidoP"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("apellidoM"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("area"), "%"+search+"%")
        ));
        jakarta.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
}
