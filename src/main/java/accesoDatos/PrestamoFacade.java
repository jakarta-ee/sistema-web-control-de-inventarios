/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Prestamo;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class PrestamoFacade extends AbstractFacade<Prestamo> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrestamoFacade() {
        super(Prestamo.class);
    }
    public List<Prestamo> find(String search){
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        return em.createQuery(
        "SELECT DISTINCT p FROM Prestamo p INNER JOIN Lectores l ON p.idSolicitante.idlector = l.idlector WHERE p.id = ?1 OR p.idLibro.titulo LIKE ?2 OR l.nombre LIKE ?3 OR l.apellidoM LIKE ?4 OR l.apellidoP LIKE ?5 ")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%")
        .setParameter(3, "%"+search+"%")
        .setParameter(4, "%"+search+"%")
        .setParameter(5, "%"+search+"%")
        .getResultList();
    }
 
    public List<Prestamo> findRange(int[] range, String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        jakarta.persistence.Query q = em.createQuery(
        "SELECT DISTINCT p FROM Prestamo p INNER JOIN Lectores l ON p.idSolicitante.idlector = l.idlector WHERE p.id = ?1 OR p.idLibro.titulo LIKE ?2 OR l.nombre LIKE ?3 OR l.apellidoM LIKE ?4 OR l.apellidoP LIKE ?5 ")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%")
        .setParameter(3, "%"+search+"%")
        .setParameter(4, "%"+search+"%")
        .setParameter(5, "%"+search+"%");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
        // Ejecutar consultas nativas
    public List<Object[]> executeQuery(String query){
        Query nativeQuery = em.createNativeQuery(query);
        return nativeQuery.getResultList();
    }
    
}
