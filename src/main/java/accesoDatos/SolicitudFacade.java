/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Solicitud;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class SolicitudFacade extends AbstractFacade<Solicitud> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SolicitudFacade() {
        super(Solicitud.class);
    }
    
    public List<Solicitud> find(String search){
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        return em.createQuery(
        "SELECT DISTINCT s FROM Solicitud s INNER JOIN Lectores l ON s.idSolicitante.idlector = l.idlector WHERE s.id = ?1 OR s.idLibro.titulo LIKE ?2 OR l.nombre LIKE ?3 OR l.apellidoM LIKE ?4 OR l.apellidoP LIKE ?5 ")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%")
        .setParameter(3, "%"+search+"%")
        .setParameter(4, "%"+search+"%")
        .setParameter(5, "%"+search+"%")
        .getResultList();
    }
 
    public List<Solicitud> findRange(int[] range, String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        jakarta.persistence.Query q = em.createQuery(
        "SELECT DISTINCT s FROM Solicitud s INNER JOIN Lectores l ON s.idSolicitante.idlector = l.idlector WHERE s.id = ?1 OR s.idLibro.titulo LIKE ?2 OR l.nombre LIKE ?3 OR l.apellidoM LIKE ?4 OR l.apellidoP LIKE ?5 ")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%")
        .setParameter(3, "%"+search+"%")
        .setParameter(4, "%"+search+"%")
        .setParameter(5, "%"+search+"%");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
    public List<Solicitud> findPendientes(){
        return em.createQuery(
        "SELECT s FROM Solicitud s WHERE s.status = 3")
        .getResultList();
    }
    public List<Solicitud> findLector(int idL){
        return em.createQuery(
        "SELECT s FROM Solicitud s WHERE s.idSolicitante.idlector = ?1")
        .setParameter(1, idL)
        .getResultList();
    }
 
    
}
