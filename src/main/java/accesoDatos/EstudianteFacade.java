/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Estudiante;
import java.util.List;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class EstudianteFacade extends AbstractFacade<Estudiante> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    public List<Estudiante> buscar(String matricula, String password){
        return em.createQuery(
        "SELECT e FROM Estudiante e WHERE e.matricula = ?1 AND e.password = ?2")
        .setParameter(1, matricula)
        .setParameter(2, password)
        .getResultList();
    }
    public List<Estudiante> buscarMatricula(String matricula){
        return em.createQuery(
        "SELECT e FROM Estudiante e WHERE e.matricula = ?1")
        .setParameter(1, matricula)
        .getResultList();
    }

    public EstudianteFacade() {
        super(Estudiante.class);
    }
     public List<Estudiante> find(String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        jakarta.persistence.criteria.Root<Estudiante> rt = cq.from(Estudiante.class);
        cq.select(rt);
        cq.where(getEntityManager().getCriteriaBuilder().or(
            getEntityManager().getCriteriaBuilder().like(rt.get("matricula"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("nombre"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("apellidoM"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("apellidoP"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("carrera"), "%"+search+"%")
        ));
        jakarta.persistence.Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
    public List<Estudiante> findRange(int[] range, String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        jakarta.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        jakarta.persistence.criteria.Root<Estudiante> rt = cq.from(Estudiante.class);
        cq.select(rt);
        cq.where(getEntityManager().getCriteriaBuilder().or(
            getEntityManager().getCriteriaBuilder().like(rt.get("matricula"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("nombre"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("apellidoM"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("apellidoP"), "%"+search+"%"),
            getEntityManager().getCriteriaBuilder().like(rt.get("carrera"), "%"+search+"%")
        ));
        jakarta.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
}
